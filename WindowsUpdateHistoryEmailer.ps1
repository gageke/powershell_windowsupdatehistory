<#
.SYNOPSIS
  This script is used to display or email the patch history of a computer.
.DESCRIPTION
  This script can be run in three modes.
  Test mode will output the update history for the local server without attempting to generate an email.
  Credentials mode is used to read in the email server credentials and save them to disk in an encrypted file so email mode can use them later.
  Email mode reads the saved credentials from disk, checks the machines update history, and attempts to email it.
  
  Test and Credential mode are intended to be run interactively with a user.  Email mode can be run automically via a scheduled task.
.INPUTS
  The inputs required for this script include the mode which can be Credentials, Email, or Test.
  Test and Email mode have an optional parameter for HistoryCount which tells the script how many records from the update history to ouput.  The default HistoryCount is 5
  Test mode has no required parameters.
  Credenials mode has an optional paramater CredentialFilePath
  Email mode requires EmailTo, EmailFrom, SmtpServerAddress, and has optional paramaters for CredentialFilePath, SmtoServerPort and UseSsl
  
.NOTES
  Version:        1.1
  Author:         Kevin Gage
  Creation Date:  12/16/2018
  Purpose/Change: Initial script development
 		Version		Release Date    Description
		=======		============    ===========
		1.0			10/16/2018		Initial script development
		1.1			10/26/2018		Added History Count.  Fixed double output
.EXAMPLE
  Before proceeding with this script, ensure Get-ExecutionPolicy is set to RemoteSigned
	- To set this, type "Set-ExecutionPolicy RemoteSigned" and press enter. When prompted, select [A] for all. 
	- Since the file was downloaded from the internet, right-click the script and click "Properties". Verify the script is unblocked by checking "Unblock" if needed.
  
  To run Test mode
    ./WindowsUpdateHistoryEmailer.ps1 -Test -HistoryCount 5
  To run Credential mode
    ./WindowsUpdateHistoryEmailer.ps1 -Credentials -CredentialFilePath 'c:\users\administrator\documents\WindowsUpdateHistoryEmailerCredentials.txt'
  To run Email mode
    ./WindowsUpdateHistoryEmailer.ps1 -Email -HistoryCount 5 -EmailTo "recipient@domain.com" -EmailFrom "sender@domain.com" -SmtpServerAddress "smtp.office365.com" -CredentialFilePath 'c:\users\administrator\documents\WindowsUpdateHistoryEmailerCredentials.txt' -SmtoServerPort 587 -UseSsl $true
#>

[CmdletBinding(DefaultParameterSetName = "Test")]
param (
    [Parameter(ParameterSetName="Test")][switch]$Test,
    [Parameter(ParameterSetName="Credentials")][switch]$Credentials,
    [Parameter(ParameterSetName="Email")][switch]$Email,

    [Parameter(ParameterSetName="Test")]
    [Parameter(ParameterSetName="Email")]
    [Parameter(ParameterSetName="Credentials")]
    [int]$HistoryCount = 5,
    [Parameter(ParameterSetName="Email", Mandatory=$true)]
    [string]$EmailTo = '',
    [Parameter(ParameterSetName="Email", Mandatory=$true)]
    [string]$EmailFrom = '',
    [Parameter(ParameterSetName="Email", Mandatory=$true)]
    [string]$SmtpServerAddress = '',
    [Parameter(ParameterSetName="Email")]
    [int]$SmtoServerPort = 587,
    [Parameter(ParameterSetName="Email")]
    [bool]$UseSsl = $true,
    [Parameter(ParameterSetName="Email")]
    [Parameter(ParameterSetName="Credentials")]
    [string]$CredentialFilePath = 'c:\users\administrator\documents\WindowsUpdateHistoryEmailerCredentials.txt'
)

Function SendEmail
{
    Param ($Subject, $Body)

    $SecurePassword = Get-Content $CredentialFilePath | ConvertTo-SecureString

    $Credentials = New-Object System.Management.Automation.PSCredential ($EmailFrom, $SecurePassword)
    Send-MailMessage -To $EmailTo -Body $Body -From $EmailFrom -Subject $Subject -SmtpServer $SmtpServerAddress -Port $SmtoServerPort -UseSsl -Credential $Credentials 
}

function Convert-WuaResultCodeToName
{
    param( [Parameter(Mandatory=$true)]
        [int] $ResultCode
    )

    $Result = $ResultCode
    
    switch($ResultCode)
    {
        2
        {
            $Result = "Succeeded"
        }
        3
        {
            $Result = "Succeeded With Errors"
        }
        4
        {
            $Result = "Failed"
        }
    }

    return $Result
}

function Get-WuaHistory 
{
    # Get a WUA Session
    $session = (New-Object -ComObject 'Microsoft.Update.Session')
    
    # Query the  History starting with the first recordp
    $history = $session.QueryHistory("",0,$HistoryCount) | ForEach-Object {
        $Result = Convert-WuaResultCodeToName -ResultCode $_.ResultCode
    
        # Make the properties hidden in com properties visible.
        $_ | Add-Member -MemberType NoteProperty -Value $Result -Name Result
        $Product = $_.Categories | Where-Object {$_.Type -eq 'Product'} | Select-Object -First 1 -ExpandProperty Name
        $_ | Add-Member -MemberType NoteProperty -Value $_.UpdateIdentity.UpdateId -Name UpdateId
        $_ | Add-Member -MemberType NoteProperty -Value $_.UpdateIdentity.RevisionNumber -Name RevisionNumber
        $_ | Add-Member -MemberType NoteProperty -Value $Product -Name Product -PassThru
    }
    
    #Remove null records and only return the fields we want
    $history |
    Where-Object {![String]::IsNullOrWhiteSpace($_.title)} |
    Select-Object Result, Date, Title, SupportUrl, Product, UpdateId, RevisionNumber
}

#Main
switch ($PSCmdlet.ParameterSetName)
{
    "Test"{
        Get-WuaHistory | Sort-Object Date -Descending | Format-Table
    }
    "Credentials"{
        $UserCredential = Get-Credential
        $UserCredential.Password | ConvertFrom-SecureString | Out-File $credentialFilePath
    }
    "Email" {
        $Subjext = $env:computername + " Update History"
        $Body = Get-WuaHistory | Sort-Object Date -Descending | Format-Table | Out-String
        
        SendEmail -subject $Subjext -body $Body
    }
    "__AllParameterSets" {
        "Unknown mode..."
    }
}